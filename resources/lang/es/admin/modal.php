<?php

return [
    'create' => 'Crear',
    'edit' => 'Editar',
    'reset' => 'Restablecer',
    'cancel' => 'Cancelar',
    'general' => 'General',
    'title' => 'Titulo',
    'new' => 'Nuevo',
    'delete' => 'Borrar',
    'items' => 'Items',
    'delete_message' => 'Estas seguro de borrar este item?',
];