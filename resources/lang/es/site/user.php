<?php

return [
	// Login 
	'login' => 'Iniciar sesión',
	'login_to_account' => 'Iniciar Sesión',
	'submit' => 'Enviar',
	'register' => 'Crear una cuenta',
	'remember' => 'Recuerdame',
	'register' => 'Registro',
	'confirmation_required' => 'Debe confirmar el Contraseña',
	'home' => 'Home',
	'email_required' => 'You need to provide a valid email address',
	'email_unique' => 'Someone else has taken this email address',
	'name_required' => 'You need to provide your name',
	'password_required' => 'You need to provide your password',
	'password_confirmed' => 'You need to provide confirm password',
    'change_password' => 'Cambiar Contraseña',
    'password_lost'	=> 'Olvido Su Contraseña?',
    // Registro 
	'name' => 'Nombre',
	'username' => 'Nombre de Usuario',
	'e_mail' => 'Dirección E-Mail',
	'password' => 'Contraseña',
	'password_confirmation' => 'Confirmar Contraseña',
    'btn-register' => 'Registrase',
];
