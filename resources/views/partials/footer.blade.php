<div id="footer">
    <div class="container">
        <p class="text-muted credit">
        	<span style="text-align: left; float: left">
        		&copy; 2015 <a href="#">Laravel 5 Starter Site</a>
        	</span>
        	<span class="hidden-phone" style="text-align: right; float: right">
        		Powered by: <a href="http://www.bri.cl/" alt="Colibri">Colibri</a>
        	</span>
        </p>
    </div>
</div>