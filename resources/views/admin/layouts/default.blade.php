@extends('layouts.sidenav')

{{-- Web site Title --}}
@section('title') Administration :: @parent @endsection

@section('home-styles')
@stop

{{-- Styles --}}
@section('styles')
    @parent

    <!-- <link href="{{ asset('css/admin.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('assets/admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link href="{{ asset('assets/admin/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adminApp.css') }}" rel="stylesheet">

@endsection

{{-- MainNavBar --}}
@section('mainbar')
@stop

{{-- Sidebar --}}
@section('sidebar')
    @include('admin.partials.nav')
@endsection

{{-- Topbar --}}
@section('topbar')
    @include('admin.partials.top')
@endsection

{{-- Scripts --}}
@section('scripts')
    @parent
    <script src="{{ asset('js/admin.js') }}"></script>

    {{-- Not yet a part of Elixir workflow --}}
    <script src="{{asset('assets/admin/js/bootstrap-dataTables-paging.js')}}"></script>
    <script src="{{asset('assets/admin/js/datatables.fnReloadAjax.js')}}"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>
    <script src="{{asset('js/Chart.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/modal.js')}}"></script>
    <script src="{{asset('assets/admin/js/basicTemplate.js')}}"></script>
    {{-- Default admin scripts--}}

    <script type="text/javascript">        
        $(function () {
            $('.metismenu > ul').metisMenu();
        });
    </script>


@endsection
