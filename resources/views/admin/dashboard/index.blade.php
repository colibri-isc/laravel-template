@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {{{ $title }}} :: @parent @stop

@section('summary')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2> {{$title}} </h2>
        <!--ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Extra Pages</a>
            </li>
            <li class="active">
                <strong>Profile</strong>
            </li>
        </ol-->
    </div>
    <div class="col-lg-2">

    </div>
</div>
@stop

{{-- Content --}}
@section('main')
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    {{-- <span class="label label-warning pull-right">Monthly</span> --}}
                    {{-- <span class="label label-info pull-right">Annual</span> --}}
                    {{-- <span class="label label-primary pull-right">Today</span> --}}
                    {{-- <span class="label label-danger pull-right">Low value</span> --}}
                    <h5>{{ trans("admin/admin.users") }}</h5>
                </div>
                <a href="{{URL::to('admin/users')}}">
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <i class="fa fa-users"></i>
                            <span class="pull-right">{{$users}}</span>
                        </h1>
                        
                        <small class="title-category">Total {{ trans("admin/admin.users") }}</small>
                    </div>
                    {{-- <div class="ibox-content">
                        <h1 class="no-margins">275,800</h1>
                        <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                        <small>New orders</small>
                    </div> --}}
                </a>
            </div>
        </div>
    </div>
    
    <div class="row">
        <!--div class="col-lg-3 col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-bullhorn fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$newscategory}}</div>
                            <div>{{ trans("admin/admin.news_categories") }}!</div>
                        </div>
                    </div>
                </div>
                <a href="{{URL::to('admin/newscategory')}}">
                    <div class="panel-footer">
                        <span class="pull-left">{{ trans("admin/admin.view_detail") }}</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-list fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$news}}</div>
                            <div>{{ trans("admin/admin.news_items") }}!</div>
                        </div>
                    </div>
                </div>
                <a href="{{URL::to('admin/news')}}">
                    <div class="panel-footer">
                        <span class="pull-left">{{ trans("admin/admin.view_detail") }}</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-list fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$photoalbum}}</div>
                            <div>{{ trans("admin/admin.photo_albums") }}!</div>
                        </div>
                    </div>
                </div>
                <a href="{{URL::to('admin/photoalbum')}}">
                    <div class="panel-footer">
                        <span class="pull-left">{{ trans("admin/admin.view_detail") }}</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-camera fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$photo}}</div>
                            <div>{{ trans("admin/admin.photo_items") }}!</div>
                        </div>
                    </div>
                </div>
                <a href="{{URL::to('admin/photo')}}">
                    <div class="panel-footer">
                        <span class="pull-left">{{ trans("admin/admin.view_detail") }}</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-list fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$videoalbum}}</div>
                            <div>{{ trans("admin/admin.video_albums") }}!</div>
                        </div>
                    </div>
                </div>
                <a href="{{URL::to('admin/videoalbum')}}">
                    <div class="panel-footer">
                        <span class="pull-left">{{ trans("admin/admin.view_detail") }}</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-facetime-video fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$video}}</div>
                            <div>{{ trans("admin/admin.video_items") }}!</div>
                        </div>
                    </div>
                </div>
                <a href="{{URL::to('admin/video')}}">
                    <div class="panel-footer">
                        <span class="pull-left">{{ trans("admin/admin.view_detail") }}</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="glyphicon glyphicon-user fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$users}}</div>
                            <div>{{ trans("admin/admin.users") }}!</div>
                        </div>
                    </div>
                </div>
                <a href="{{URL::to('admin/users')}}">
                    <div class="panel-footer">
                        <span class="pull-left">{{ trans("admin/admin.view_detail") }}</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div!-->
    </div>
    @endsection

@section('scripts')
    @parent

    <script type="text/javascript">
        setTimeout(function() {
          var name = '{{trans("site/site.welcome")}}{{Auth::user()->name}}';
          toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: false,
            progressBar: true,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            onclick: null,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "3000",
            extendedTimeOut: "10000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "slideDown",
            hideMethod: "fadeOut"
          };
          toastr.success('Administration Panels', name);

        }, 1300);        
    </script>

@stop
