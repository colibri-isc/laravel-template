@extends('app')
@section('content')
    <div class="row">
            @yield('top')
    </div>
    <div class="row">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                @yield('sidebar')                
            </div>
        </nav>
        
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                @yield('topbar')                
            </div>

            {{-- Summary --}}
            @yield('summary')                
            

            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content">
                        @yield('main')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
